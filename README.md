# Welcome to DevOps Training.

As a begineer to the you need to create an account in AWS in order to proceed with the lab practice.

Following are the list of setups you need to perform.

## a. Create AWS Account.

### 1. You need to create an account by opening the following URL and click on "Create Free Account"

http://aws.amazon.com

![Screenshot_2021-09-06_at_10.08.48_PM](/uploads/e41bb544dfbe151ad61f398915aaed24/Screenshot_2021-09-06_at_10.08.48_PM.png)

### 2. Fill your details to create an account. Details required are Email address and your account name of your choice.

### 3. After submitting the data you will be taken to SignUp form which requires more details.
    i. Select Account Type as `Personal`
    ii. Your address and Mobile number. (Need to provide a valid mobile number, You will recive a call in further setup)

![Screenshot_2021-09-06_at_10.48.04_PM](/uploads/1596f75218f114b6bc13f6aff34d186c/Screenshot_2021-09-06_at_10.48.04_PM.png)

### 4. Next you will be prompt to provide the credit card details. So proceed by providing the details
    i. Provide credit/debit card details.
    ii. Select PAN number as NO
    

![Screenshot_2021-09-06_at_10.50.06_PM](/uploads/bfa91d82b56b132b5e66ed1c970b8aa9/Screenshot_2021-09-06_at_10.50.06_PM.png)

### 5. You will be redirected to your bank payment portal to provide OTP and after that select the plan as `Basic`.

### 6. Once after your account creation is completed then use the following link to open your account.

http://console.aws.amazon.com

![Screenshot_2021-09-06_at_10.51.35_PM](/uploads/268bd630e0d0486a7e91d88723c09797/Screenshot_2021-09-06_at_10.51.35_PM.png)


## b. Create a server in AWS.

Choose a region of your choice. (N.Virginia, Ohio, or Oregon)

Choose a service called as `EC2` and create a Server as shown.

![Screenshot_2021-09-06_at_10.54.21_PM](/uploads/0fce6ae61bb8d5cd983cc1e726b99263/Screenshot_2021-09-06_at_10.54.21_PM.png)

![Screenshot_2021-09-06_at_10.55.18_PM](/uploads/f8baca4009cb2803361abd89477f8af3/Screenshot_2021-09-06_at_10.55.18_PM.png)

![Screenshot_2021-09-06_at_10.56.39_PM](/uploads/e5bb2e02950d362c34231bffe4df3005/Screenshot_2021-09-06_at_10.56.39_PM.png)

Owner: 973714476881   (OR) Click following URL 

N.Virginia Region: https://console.aws.amazon.com/ec2/home?region=us-east-1#Images:visibility=public-images;ownerAlias=973714476881;sort=name

Oregon Region : https://console.aws.amazon.com/ec2/home?region=us-west-2#Images:visibility=public-images;ownerAlias=973714476881;sort=name

Ohio Region : https://console.aws.amazon.com/ec2/home?region=us-east-2#Images:visibility=public-images;ownerAlias=973714476881;sort=name

Then select the AMI and click on "Launch"

![Screenshot_2021-09-06_at_10.57.35_PM](/uploads/fa09db96748ba0d0ed12753091ca2cc0/Screenshot_2021-09-06_at_10.57.35_PM.png)

Select "t2.micro" and proceed.


![Screenshot_2021-09-06_at_10.58.48_PM](/uploads/1e787cc2e3d179173f5251d012ff8c4b/Screenshot_2021-09-06_at_10.58.48_PM.png)

No changes required, Just Click on "Add Storage"

![Screenshot_2021-09-06_at_10.59.39_PM](/uploads/e16838720a7458e4170cb946f0cdeda7/Screenshot_2021-09-06_at_10.59.39_PM.png)

No changes required , Click on "Add Tags"

![Screenshot_2021-09-06_at_11.01.32_PM](/uploads/9b97930d8101d630a4e18b138113fcd8/Screenshot_2021-09-06_at_11.01.32_PM.png)

Click on "Next: Configure Security Group"


![Screenshot_2021-09-06_at_11.03.11_PM](/uploads/43c5895df0d8b8ccd6e409408243d6e8/Screenshot_2021-09-06_at_11.03.11_PM.png)


Finally Click on "Review and Launch" and "Launch"

![Screenshot_2021-09-06_at_11.04.57_PM](/uploads/82beeb62c42ca6b020c4e2a60b113451/Screenshot_2021-09-06_at_11.04.57_PM.png))

Select "Proceed with out KeyPair"

Then use the Public IP of the server and connect it with Putty , Using the following credentials.

Username / Password : root / DevOps321


### 

### Download and Install putty

https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html

For Mac Users, Use terminal

`ssh root@<Public-IP>`

### Install GitBash

`MAC and Linux Users can ignore this installation`

Download the GitBash from https://git-scm.com/downloads and install it with default options

Then,

Open the GitBash and run the following commands. But Mac and Linux Users can use `Terminal` to run these commands.

`$ ssh-keygen -f devops `   <- You will be asked to enter Passphrase prompt , But just hit ENTER.

`$ mv devops devops.pem `

![Screenshot_2021-09-06_at_11.06.15_PM](/uploads/99b97566a07c804c20da072b8275d5d5/Screenshot_2021-09-06_at_11.06.15_PM.png)

## Upload `devops.pub` file into AWS.


Lab Practice Katacoda Link: https://www.katacoda.com/rkalluru
